//console.log("Hello World!");

//[Section] objects

let cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
}

console.log("Result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);


//"this" inside an object
function Laptop(name, manufacturedDate){
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using object constructors");
console.log(laptop);


// re-using laptop function

let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using object constructors");
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating objects using object constructors");
console.log(oldLaptop);


// Creating empty object

let computer = {};
let myComputer = new Object();

// [section] Accessing Object Properties
// DOT NOTATION --> to access particular property in an object.

console.log("Result from dot notation: " + myLaptop.name);
console.log("Result from dot notation: " + myLaptop.manufacturedDate);

// Square bracket notation
console.log("Result from dot notation: " + myLaptop['name']);

//Accessing array objects

let array = [laptop, myLaptop];
console.log(array[0]['name']);
console.log(array[0].name);

// [section] initializing, adding, deleting, re-assigning object properties

let car = {};
console.log(car);

// Initializing or adding property --> using dot notation

car.name = "Honda Civic";
console.log("Result from adding a property using dot notation:");
console.log(car);

/*car.manufacturedDate = "2019";
console.log("Result from adding a property using dot notation:");
console.log(car);*/

//Initializing or adding property using bracket notation

car['manufacturedDate'] = 2019;
console.log(car.manufacturedDate);
console.log("Result from adding a property using bracket notation:");
console.log(car);

// delete object property
// delete car['manufacturedDate'];
delete car.manufacturedDate;
console.log("Result from deleting a properties:");
console.log(car);

//re-assigning object properties --> modifying/updating
car.name = "Dodge Charger R/T";
console.log("Result from re-assigning properties:")
console.log(car);


//[Section] Object Methods

let person = {
	name: "Charloyd",
	talk: function(){
		console.log("Hello my name is " + this.name);
	}
}

console.log(person);
console.log("Result from object method");
person.talk(); //invoke/calling a function

// Add a property with an object method/function

person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}

person.walk();

// Another example
// Nested properties
let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas"
	},
	emails:["joe@mail.com", "joesmith@mail.xyz"],
	introduce: function(){
		console.log("Hello, my name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();


//[section] real world application of objects

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tacked targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	},
	faint: function(){
		console.log("pokemon fainted");
	}

}

console.log(myPokemon);


// Creating an object constructor to us on the process "this" and "new"

function Pokemon(name, level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 2 * level; //32
	this.attack = level; //3

	//Methods/Function
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now to _targetPokemonHealth_");
	}
	this.faint = function() {
		console.log(this.name + " fainted");
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);